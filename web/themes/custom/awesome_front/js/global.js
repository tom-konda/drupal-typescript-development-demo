(Drupal => {
  const headerMenu = document.querySelector('#header-menu');

  if (headerMenu) {
    once('awesome_front', headerMenu).forEach(element => {
      element.addEventListener('pointerdown', ({
        x,
        y
      }) => {
        console.log([x, y]);
      });
    });
  }

  Drupal.theme.awesome = text => {
    const divElement = document.createElement('div');
    divElement.textContent = text ?? 'default text';
    return divElement;
  };
})(Drupal);