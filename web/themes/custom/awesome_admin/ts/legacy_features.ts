(($, Drupal) => {
  Drupal.behaviors.legacyFeature = {
    attach: (context, settings) => {
      const { legacyFeature } = settings as {
        legacyFeature: { settings1: number };
      };
      $('#hogehoge')
        .once('legacy_features')
        .on('click', (event: JQuery.ClickEvent) => {
          const ajax = Drupal.ajax({});
          const deprecatedSettings = ajax.element_settings;
          console.log(event.target);
          $.cookie('hogehoge', 'foo');
        });
    },
  };
})(jQuery, Drupal);
