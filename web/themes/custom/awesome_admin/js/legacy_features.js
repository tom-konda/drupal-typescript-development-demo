(($, Drupal) => {
  Drupal.behaviors.legacyFeature = {
    attach: (context, settings) => {
      const {
        legacyFeature
      } = settings;
      $('#hogehoge').once('legacy_features').on('click', event => {
        const ajax = Drupal.ajax({});
        const deprecatedSettings = ajax.element_settings;
        console.log(event.target);
        $.cookie('hogehoge', 'foo');
      });
    }
  };
})(jQuery, Drupal);