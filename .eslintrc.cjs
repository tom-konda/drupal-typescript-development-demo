const coreSettings = require("./web/core/.eslintrc.json");

const projectSettings = {
  ...coreSettings,
  ...{
    ignorePatterns: ["**/vendor", "./web/core", "./web/sites", "**/ts/*.d.ts"],
    parser: "@typescript-eslint/parser",
    plugins: ["@typescript-eslint"],
  },
};

projectSettings.extends = [
  "airbnb",
  "plugin:prettier/recommended",
  "plugin:@typescript-eslint/eslint-recommended",
  "plugin:@typescript-eslint/recommended",
];

module.exports = projectSettings;
