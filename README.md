# Drupal TypeScript Development Demo

## これは何

Non-decoupled Drupal でのフロントエンド開発において、TypeScript を使用する際のライブラリや
設定ファイルなどの置き場所などを示したデモレポジトリです。

## 事前準備

レポジトリのルートディレクトリで下記を行います。

1. Composer で `composer install` を実行する
2. yarn で `yarn install` を実行する
3. yarn で各ワークスペースごとに `yarn workspace WORKSPACE_NAME install` を実行する
  - ワークスペース名の一覧
    - great_site_features
    - awesome_admin
    - awesome_front

## 使い方
 
`yarn build` または `yarn build:legacy` を実行します。
`yarn build:legacy` を実行すると、古めの JavaScript が出力されます。（コミットしているコードは新しい JavaScript コードです）